package com.example.santicovi.ellistadodecartas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by SantiCovi on 10/12/2017.
 */

public class DetailActivityFragment extends Fragment {
    private View view;
    private FragmentDetailBinding binding;

    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if (i != null) {
            Cartas carta = (Cartas) i.getSerializableExtra("carta");

            if (carta != null) {
                updateUi(carta);
            }
        }

        return view;
    }

    private void updateUi(Cartas carta) {
        Log.d("CARTAS", carta.toString());

        Glide.with(getContext()).load(carta.getImagenURL()).into(binding.imageCarta);

    }
}
