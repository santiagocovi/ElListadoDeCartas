package com.example.santicovi.ellistadodecartas;

import android.content.Context;

/**
 * Created by SantiCovi on 10/12/2017.
 */

@Database(entities = {Cartas.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "db")
                            .build();
        }
        return INSTANCE;
    }

    public abstract CartasDAO getMovieDao();
}
