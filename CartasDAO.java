package com.example.santicovi.ellistadodecartas;

import java.util.List;

/**
 * Created by SantiCovi on 10/12/2017.
 */


@Dao
public interface CartasDAO {
    @Query("select * from cartas")
    LiveData<List<Cartas>> getCartas();

    @Insert
    void addCarta(Cartas carta);

    @Insert
    void addCartas(List<Cartas> cartas);

    @Delete
    void deleteCarta(Cartas carta);

    @Query("DELETE FROM cartas")
    void deleteCartas();


}
