package com.example.santicovi.ellistadodecartas;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SantiCovi on 10/12/2017.
 */

public class CartasViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CartasDAO cartasDAO;


    public CartasViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.cartasDAO = appDatabase.getMovieDao();
    }

    public LiveData<List<Cartas>> getCartas() {
        return cartasDAO.getCartas();
    }

    public void reload() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Cartas>> {
        @Override
        protected ArrayList<Cartas> doInBackground(Void... voids) {

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
            String color = preferences.getString("Color", " ");

            MagicAPI api = new MagicAPI();
            ArrayList<Cartas> result = null;

            Log.d("DEBUG", color);

            if (!color.equals(" ")) {
                result = api.getColorCartas(color);
            } else {
                result = api.get100Cartas();
            }

            Log.d("DEBUG", result.toString());

            cartasDAO.deleteCartas();
            cartasDAO.addCartas(result);

            return result;
        }

    }
}